# Ember-pt-config

This add-on provides:

+ Pitchtarget authenticator for `ember-simple-auth`
+ `ptApi` service to do authenticated API call to the Pitchtarget APIs

The `ptApi` service is injected on every route by an initializer. This service depends
on a `session` service to be available: at the moment this must be a service following
the `ember-simple-auth` session service interface.

## Installation

* Add to your package.json `"ember-pt-config": "https://bugant@bitbucket.org/pitchtarget/ember-pt-config.git"`
* `npm install`

## Configuration

The `ptApi` service needs to know the API base URL and the client ID. These have to be
defined in your app's configuration file (`config/environment.js`):

```javasript
  ptApi: {
    baseUrl: 'https://my-api.com'
    clientId: 'bha8719IHDEW78'
  }
```

# Contributing

## Development install
* `git clone` this repository
* `npm install`
* `bower install`

## Running

* `ember server`
* Visit your app at http://localhost:4200.

## Running Tests

* `npm test` (Runs `ember try:testall` to test your addon against multiple Ember versions)
* `ember test`
* `ember test --server`

## Building

* `ember build`

For more information on using ember-cli, visit [http://ember-cli.com/](http://ember-cli.com/).
