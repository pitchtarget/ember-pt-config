import Ember from 'ember';
const { get, set } = Ember;

export default Ember.Mixin.create({
  config: Ember.inject.service(),
  session: Ember.inject.service(),
  fb: Ember.inject.service(),
  init() {
    this._super(...arguments);
    get(this, 'fb').on('fb.setAccessToken', token => {
      set(get(this, 'session.data'), 'facebook_access_token', token);
      localStorage.setItem('facebook_access_token', token);
    });
  },
  beforeModel() {
    this._super(...arguments);

    const defaultRoute = get(this, 'config.ptApi.defaultProviderLoginRoute');

    if (!get(this, 'session.data.facebook_access_token')) {
      return this.transitionTo(defaultRoute);
    }
    const fb = get(this, 'fb');
    return fb.getLoginStatus()
      .then(res => {
        if (res.status === 'not_authorized') {
          set(get(this, 'session.data'), 'facebook_access_token', '');
          fb.setAccessToken();
          localStorage.setItem('facebook_access_token', '');
          return this.transitionTo(defaultRoute);
        }
      });
  }
});
