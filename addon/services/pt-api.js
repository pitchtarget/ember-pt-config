import Ember from 'ember';
const { get, set } = Ember;

export default Ember.Service.extend({
  config: Ember.inject.service(),
  ajax: Ember.inject.service(),
  session: Ember.inject.service(),
  notify: Ember.inject.service(),

  baseUrl: Ember.computed.alias('config.ptApi.baseUrl'),
  clientId: Ember.computed.alias('config.ptApi.clientId'),

  _url(path) {
    return `${get(this, 'baseUrl')}/${path}`;
  },

  _setAuthHeader(authData, options) {
    let optionsWithAuthHeaders = Ember.assign({}, options);
    optionsWithAuthHeaders.headers = options.headers || {};
    optionsWithAuthHeaders.headers['Authorization'] = `OAuth ${authData.access_token}`;
    return optionsWithAuthHeaders;
  },

  request(path, auth=true, options = {}) {
    let authData;
    if (auth) {
      authData = get(this, 'session.data.authenticated');
      options = this._setAuthHeader(authData, options);
    }

    return this.get('ajax').raw(this._url(path), options).then((response) => {
      return Ember.RSVP.resolve(response.payload);
    }).catch((err) => {
      const payload = err.payload;
      console.error(payload);
      get(this, 'notify').alert(payload.error_description || payload.error || payload.errors[0]);
      return Ember.RSVP.reject(payload);
    });
  }
});
