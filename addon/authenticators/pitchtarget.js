import Ember from 'ember';
import BaseAuthenticator from 'ember-simple-auth/authenticators/base';
const { get, set } = Ember;

export default BaseAuthenticator.extend({
  ajax: Ember.inject.service(),
  session: Ember.inject.service(),
  store: Ember.inject.service(),
  ptApi: Ember.inject.service(),
  fb: Ember.inject.service(),

  authenticate(email, psw, token=null) {
    const ptApi = get(this, 'ptApi');

    if (token) {
      return Ember.RSVP.resolve({access_token: token});
    }

    return ptApi.request('oauth/authorize', false, {
      method: 'POST',
      dataType: 'json',
      data: {
        username: email,
        password: psw,
        grant_type: 'password',
        client_id: get(ptApi, 'clientId')
      }
    });
  },

  invalidate() {
    let authData = get(this, 'session.data.authenticated');
    if (!authData.access_token) {
      return Ember.RSVP.reject('User not authenticated');
    }

    return get(this, 'ptApi').request('sign-out', true, {
      method: 'POST'
    }).catch((reason) => {
      console.log('Cannot logout the user on the server side.', reason);
      return Ember.RSVP.resolve();
    });
  },

  restore(data) {
    const token = localStorage.getItem('facebook_access_token');
    const ptApi = get(this, 'ptApi');
    if (token) { get(this, 'fb').setAccessToken(token); }
    return ptApi.request('me', false, ptApi._setAuthHeader(data, {})).then((response) => {
      set(get(this, 'session.data'), 'user', response.user);
      set(get(this, 'session.data'), 'fb_ad_accounts', response.fb_ad_accounts);
      return Ember.RSVP.resolve(data);
    });
  }
});
