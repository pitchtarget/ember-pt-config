export function initialize(application) {
  application.inject('route', 'ptApi', 'service:ptApi');
}

export default {
  name: 'pitchtarget',
  initialize
};
