import Ember from 'ember';
import { moduleFor, test } from 'ember-qunit';

moduleFor('service:pt-api', 'Unit | Service | pt api', {
  // Specify the other units that are required for this test.
  // needs: ['service:foo']
});

const ajaxMocked = {
  raw(url, options) {
    this.ajax_url = url;
    this.ajax_options = options;
    return Ember.RSVP.resolve({foo: 'bar'});
  }
};

const signedInSession = {
  data: {
    authenticated: {
      access_token: 'foo'
    }
  }
};

// Replace this with your real tests.
test('it exists', function(assert) {
  let service = this.subject();
  assert.ok(service);
});

test('it has a request function', function(assert) {
  assert.expect(2);
  const service = this.subject();
  assert.ok(service.request, 'Has a request property');
  assert.equal(typeof(service.request), 'function', 'Has a request property');
});

test ('it requests a URL', function(assert) {
  assert.expect(3);

  const service = this.subject({ajax: ajaxMocked});
  service.request('/foo', false);
  assert.ok(service.ajax.ajax_url, 'request for a URL');
  assert.ok(service.ajax.ajax_url.match(/\/foo/), 'it matched the requested path');
  assert.ok(service.ajax.ajax_url.match(/www.lemieapi.com/), 'it matched the configured ptApi.baseUrl');
});

test ('it requests an authenticated URL', function(assert) {
  assert.expect(3);

  const service = this.subject({ajax: ajaxMocked, session: signedInSession});
  service.request('/foo');
  assert.ok(service.ajax.ajax_url, 'request for a URL');
  assert.ok(service.ajax.ajax_url.match(/\/foo/), 'it matched the requested path');
  assert.ok(service.ajax.ajax_url.match(/www.lemieapi.com/), 'it matched the configured ptApi.baseUrl');
});

test('it add auth headers', function(assert) {
  assert.expect(2);

  const service = this.subject({ajax: ajaxMocked, session: signedInSession});
  service.request('/foo');
  assert.ok(service.ajax.ajax_options.headers, 'sets headers option');
  assert.equal(service.ajax.ajax_options.headers['Authorization'], 'OAuth foo', 'it sets the Authorization header');
});

test('it does not add auth headers for unauthenticated request', function(assert) {
  assert.expect(1);

  const service = this.subject({ajax: ajaxMocked});
  service.request('/foo', false);
  assert.ok(!service.ajax.ajax_options.headers, 'does not set headers option');
});
