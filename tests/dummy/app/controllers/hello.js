import Ember from 'ember';

export default Ember.Controller.extend({
  ptApi: Ember.inject.service(),

  baseUrl: Ember.computed.reads('ptApi.baseUrl'),
  clientId: Ember.computed.reads('ptApi.clientId')
});
